import { ApolloServer } from "apollo-server-express";
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core";
import type { DocumentNode } from "graphql";
import http from "http";
import express from "express";

import { connectMongo } from "./mongo/connection";
import { jsonExtractor } from "./graphql/jsonExtractor";
import { schemaBuilder } from "./graphql/schemaBuilder";
import { resolversBuilder } from "./graphql/resolversBuilder";

const PORT = process.env.PORT || 4000;
// TODO: Replace DATABASE_NAME by dynamic tenant name
const DATABASE_NAME = "b=smartsuite";

async function startApolloServer(schema: DocumentNode, resolvers: any) {
  const app = express();
  const httpServer = http.createServer(app);

  const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });

  await server.start();

  server.applyMiddleware({ app });

  await new Promise<void>((resolve) =>
    httpServer.listen({ port: PORT }, resolve)
  );

  console.log(
    `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`
  );
}

async function bootstrap() {
  await connectMongo(DATABASE_NAME);

  const baseJSON = await jsonExtractor({
    solutionId: "63a1c71f541f68763a37f3ac",
  });

  if (baseJSON) {
    const schema = schemaBuilder(baseJSON);
    const resolvers = resolversBuilder(baseJSON);

    if (schema && resolvers) {
      startApolloServer(schema, resolvers);
    }
  }
}

bootstrap();
