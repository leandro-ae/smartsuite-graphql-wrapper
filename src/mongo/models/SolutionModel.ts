import { Schema, model } from "mongoose";

import { Solution } from "../../entities/Solution";

const solutionSchema = new Schema<Solution>(
  {
    name: String,
    slug: String,
    template: String,
  },
  { collection: "solution" }
);

export const SolutionModel = model<Solution>("solution", solutionSchema);
