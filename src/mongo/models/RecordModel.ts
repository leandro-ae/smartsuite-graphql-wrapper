import { Schema, model } from "mongoose";

import { Record } from "../../entities/Record";

interface Builder {
  applicationId: string;
}

let schema: any = null;

export function recordModelBuilder({ applicationId }: Builder) {
  if (!schema) {
    const recordSchema = new Schema<Record>(
      {},
      {
        collection: `records_${applicationId}`,
      }
    );

    schema = recordSchema;
  }

  return model<Record>(`records_${applicationId}`, schema);
}
