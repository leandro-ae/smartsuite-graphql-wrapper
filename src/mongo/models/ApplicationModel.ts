import { Schema, model } from "mongoose";

import { Application } from "../../entities/Application";

const applicationSchema = new Schema<Application>(
  {
    solution: {
      type: Schema.Types.ObjectId,
      ref: "solution",
    },
    name: String,
    slug: String,
    order: Number,
    structure: Array,
  },
  { collection: "application" }
);

export const ApplicationModel = model<Application>(
  "Application",
  applicationSchema
);
