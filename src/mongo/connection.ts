import mongoose from "mongoose";
import { env } from "../config/env";

export async function connectMongo(database: string) {
  mongoose.set("strictQuery", false);

  await mongoose.connect(env.MONGO_URL, {
    dbName: database,
  });

  console.log(`✅ Mongo successfully connected to the database ${database}`);
}
