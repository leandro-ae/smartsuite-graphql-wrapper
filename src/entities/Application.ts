import { ObjectId } from "bson";

export interface Application {
  _id: string;
  solution: typeof ObjectId;
  name: string;
  slug: string;
  order: number;
  structure: Array<{ [key: string]: any }>;
}
