export interface Solution {
  _id: string;
  name: string;
  slug: string;
  template: string;
}
