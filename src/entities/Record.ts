export interface Record {
  _id: string;
  title: string;
  [key: string]: any;
}
