import { ObjectID } from "bson";
import { ObjectId } from "mongodb";

import { ApplicationModel } from "../mongo/models/ApplicationModel";
import { recordModelBuilder } from "../mongo/models/RecordModel";
import { SolutionModel } from "../mongo/models/SolutionModel";

async function getRecordsByApplication(applicationId: string) {
  const RecordModel = recordModelBuilder({ applicationId });
  const records = await RecordModel.find();
  return records.map((record) => record.toJSON());
}

async function getApplicationWithRecords(applicationSlug: string) {
  const document = await ApplicationModel.findOne({ slug: applicationSlug });
  const application = document?.toJSON();

  if (application) {
    const records: any[] = await getRecordsByApplication(application._id);
    return {
      ...application,
      records,
    };
  } else {
    return {};
  }
}

async function getApplicationsBySolution(solutionId: string) {
  const applications = await ApplicationModel.find({
    solution: new ObjectId(solutionId),
  });

  if (applications && applications.length > 0) {
    return applications.map((application) => application.toJSON());
  } else {
    return [];
  }
}

async function getSolutionWithApplications(solutionSlug: string) {
  const document = await SolutionModel.findOne({ slug: solutionSlug });
  const solution = document?.toJSON();

  if (solution) {
    const applications = await getApplicationsBySolution(solution._id);

    let applicationsObject: any = {};

    for (let i = 0; i < applications.length; i++) {
      const fullApplication = await getApplicationWithRecords(
        applications[i].slug
      );
      applicationsObject[applications[i].slug] = fullApplication;
    }

    return {
      ...solution,
      applications: applicationsObject,
    };
  } else {
    return {};
  }
}

export function resolversBuilder(baseJSON: any) {
  // TODO: replace solutionSlug by actual solution from query
  const [solutionSlug] = Object.keys(baseJSON);

  let queries: any = {};
  let mutations: any = {};

  baseJSON[solutionSlug].applications.forEach((application: any) => {
    queries = {
      ...queries,

      // Show Application
      [`application_${application.slug}`]: async () => {
        return await getApplicationWithRecords(application.slug);
      },

      // List Records
      [`records_${application.slug}`]: async () => {
        return await getRecordsByApplication(application.slug);
      },

      // Show Record
      [`record_${application.slug}`]: async (_: any, { recordId }: any) => {
        const RecordModel = recordModelBuilder({
          applicationId: application._id,
        });
        const document = await RecordModel.findById(recordId);
        return document?.toJSON();
      },
    };

    mutations = {
      ...mutations,

      // Create Record
      [`create_record_${application.slug}`]: async (
        _: any,
        { body }: any
      ) => ({}), // TODO: fetch API

      // Update Record
      [`update_record_${application.slug}`]: async (
        _: any,
        { recordId, body }: any
      ) => ({}), // TODO: fetch API

      // Delete Record
      [`delete_record_${application.slug}`]: async (
        _: any,
        { recordId }: any
      ) => ({}), // TODO: fetch API
    };
  });

  queries = {
    ...queries,

    solution: async () => {
      const solution = await getSolutionWithApplications(solutionSlug);
      return solution;
    },

    applications: async () => {
      const solution = (await getSolutionWithApplications(solutionSlug)) as any;
      return solution.applications;
    },
  };

  return {
    Query: {
      ...queries,
    },
    Mutation: {
      ...mutations,
    },
  };
}
