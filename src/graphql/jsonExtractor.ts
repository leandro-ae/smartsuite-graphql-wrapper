import { Types } from "mongoose";

import { ApplicationModel } from "../mongo/models/ApplicationModel";
import { SolutionModel } from "../mongo/models/SolutionModel";
import { recordModelBuilder } from "../mongo/models/RecordModel";

interface JsonExtractor {
  solutionId: string;
}

export async function jsonExtractor({ solutionId }: JsonExtractor) {
  const baseSchemaObject: any = {};
  const solution = await SolutionModel.findById(solutionId);

  if (!solution) {
    return null;
  }

  baseSchemaObject[solution.slug] = {
    // ...solution.toJSON(),
    applications: [],
  };

  const applications = await ApplicationModel.find({
    solution: new Types.ObjectId(solution?._id),
  });

  if (!applications.length) {
    return null;
  }

  for (let application of applications) {
    const applicationIndex = applications.indexOf(application);
    const { _id, name, slug, order, structure } = application.toJSON();

    baseSchemaObject[solution.slug].applications.push({
      _id,
      name,
      slug,
      order,
      structure: structure.map(({ slug, label, field_type }) => ({
        slug,
        label,
        field_type,
      })),
      records: [],
    });

    const RecordModel = recordModelBuilder({
      applicationId: application._id,
    });

    const records = await RecordModel.find();

    for (let rec of records) {
      const record = rec.toJSON();
      const recordObject: any = {};

      for (let structure of application.structure) {
        if (record[structure.slug] && structure.slug !== "s3643effbc") {
          recordObject[structure.slug] = record[structure.slug];
        }
      }

      baseSchemaObject[solution.slug].applications[
        applicationIndex
      ].records.push(recordObject);
    }
  }

  return baseSchemaObject;
}
