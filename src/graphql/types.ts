export const types = `
type Attrs {
  textAlign: String
}

type Marks {
  type: String
}

type Data {
  type: String
  content: [Data]
  attrs: Attrs
  marks: [Marks]
  text: String
}

type CustomContent {
  data: Data
  html: String
  preview: String
}

type Status {
  value: String
  updated_on: String
}

type Date {
  date: String
  include_time: Boolean
}

type DateRange {
  from_date: Date
  to_date: Date
}

type DueDateRange {
  from_date: Date
  to_date: Date
  is_overdue: Boolean
  status_is_completed: Boolean
  status_updated_on: String
}

type Timestamp {
  by: String
  on: String
}

type NumberDecimal {
  numberDecimal: String
}

type Phone {
  phone_country: String
  phone_number: String
  phone_extension: String
  phone_type: Int
  sys_root: String
  sys_title: String
}

type Address {
  location_address: String
  location_address2: String
  location_city: String
  location_state: String
  location_zip: String
  location_country: String
  location_longitude: String
  location_latitude: String
  sys_root: String
}

type FullName {
  title: String
  first_name: String
  middle_name: String
  last_name: String
  sys_root: String
}

type Metadata {
  container: String
  filename: String
  key: String
  mimetype: String
  size: Int
}

type File {
  handle: String
  file_type: String
  created_on: String
  updated_on: String
  description: String
  icon: String
  metadata: Metadata
}

type Votes {
  user_id: String
  date: String
}

type TotalVotes {
  total_votes: Int
  votes: [Votes]
}

type SocialNetwork {
  facebook_username: String
  instagram_username: String
  linkedin_username: String
  twitter_username: String
  sys_root: String
}

type Items {
  id: String
  completed: Boolean
  assignee: String
  due_date: String
  completed_at: String
  content: CustomContent
}

type SubItems {
  id: String
  name: String
  description: String
  last_updated: Timestamp
  first_created: Timestamp
  date: Date
}

type Checklist {
  total_items: Int
  completed_items: Int
  items: [Items]
}

type TimeTrackLogs {
  user_id: String
  date_time: String
  duration: Int
  time_range: String
  note: String
}

type TimeTracking {
  total_duration: Int
  time_track_logs: [TimeTrackLogs]
}

type Signature {
  text: String
  drawing: [String]
}

type ColorPicker {
  value: String
  name: String
}

type IPAddress {
  country_code: String
  address: String
}
`;

export const match: { [key: string]: string } = {
  recordtitlefield: "String",
  richtextareafield: "CustomContent",
  userfield: "[String]",
  statusfield: "Status",
  duedatefield: "DateRange",
  singleselectfield: "String",
  firstcreatedfield: "Timestamp",
  lastupdatedfield: "Timestamp",
  commentscountfield: "Int",
  autonumberfield: "Int",
  textfield: "String",
  textareafield: "String",
  multipleselectfield: "[String]",
  datefield: "Date",
  daterangefield: "DateRange",
  timefield: "String",
  numberfield: "NumberDecimal",
  numbersliderfield: "Int",
  percentfield: "NumberDecimal",
  currencyfield: "NumberDecimal",
  emailfield: "[String]",
  phonefield: "Phone",
  addressfield: "Address",
  fullnamefield: "FullName",
  linkfield: "[String]",
  yesnofield: "Boolean",
  filefield: "File",
  linkedrecordfield: "[String]",
  ratingfield: "Int",
  votefield: "TotalVotes",
  tagsfield: "[String]",
  socialnetworkfield: "SocialNetwork",
  durationfield: "NumberDecimal",
  percentcompletefield: "Int",
  checklistfield: "Checklist",
  timetrackingfield: "TimeTracking",
  formulafield: "NumberDecimal",
  recordidfield: "String",
  signaturefield: "Signature",
  colorpickerfield: "[ColorPicker]",
  ipaddressfield: "[IPAddress]",
  subitemsfield: "SubItems",
  countfield: "NumberDecimal",
  rollupfield: "NumberDecimal",
  lookupfield: "String", // TODO: validate this
  buttonfield: "String", // TODO: validate this
};
