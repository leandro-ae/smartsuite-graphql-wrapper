// @ts-ignore
import { jsonToSchema } from "@walmartlabs/json-to-simple-graphql-schema/lib";
import { gql } from "apollo-server-express";
import { pascalCase } from "pascal-case";

import { types, match } from "./types";

export function schemaBuilder(baseJSON: any) {
  const [solutionObject] = Object.values(baseJSON);
  const { applications } = solutionObject as any;
  let baseSchema = types;

  for (let application of applications) {
    const { structure: structures, slug, _id } = application;

    let recordType = `type Record_${pascalCase(slug)} {`;

    for (let structure of structures) {
      const { slug: structureSlug, field_type } = structure as {
        slug: string;
        field_type: string;
      };
      const typeMatch = match[field_type];
      const fieldType = `${structureSlug}: ${typeMatch}`;
      recordType += `\n  ${fieldType}`;
    }

    recordType += `\n}\n`;

    const applicationType = `type ${pascalCase(
      slug
    )} {\n  _id: String\n  name: String\n  slug: String\n  order: Int\n  records: [Record_${pascalCase(
      slug
    )}]\n}\n`;

    baseSchema += `\n${recordType}\n${applicationType}`;
  }

  const [solutionSlug] = Object.keys(baseJSON);

  baseSchema += `\ntype Applications {`;
  baseJSON[solutionSlug].applications.forEach((application: any) => {
    baseSchema += `\n  ${application.slug}: ${pascalCase(application.slug)}`;
  });
  baseSchema += `\n}\n\n`;

  const solutionType = `type ${pascalCase(
    solutionSlug
  )} {\n  applications: Applications\n}\n`;

  baseSchema += `${solutionType}`;

  let queries = "";
  let mutations = "";

  baseJSON[solutionSlug].applications.forEach((application: any) => {
    /**
     * Queries
     */
    // Show Application
    queries += `application_${application.slug}: ${pascalCase(
      application.slug
    )}\n`;

    // List Records
    queries += `records_${application.slug}: [Record_${pascalCase(
      application.slug
    )}]\n`;

    // Show Record
    queries += `record_${
      application.slug
    }(recordId: String): Record_${pascalCase(application.slug)}\n`;

    /**
     * Mutations
     */
    // Create Record
    mutations += `create_record_${application.slug}: Record_${pascalCase(
      application.slug
    )}\n`;

    // Update Record
    mutations += `update_record_${
      application.slug
    }(recordId: String): Record_${pascalCase(application.slug)}\n`;

    // Delete Record
    mutations += `delete_record_${
      application.slug
    }(recordId: String): Record_${pascalCase(application.slug)}\n`;
  });

  const fullSchema = gql`
    ${baseSchema}

    type Query {
      solution: ${pascalCase(solutionSlug)}
      applications: Applications
      ${queries}
    }

    type Mutation {
      ${mutations}
    }
  `;

  return fullSchema;
}
