export interface CustomValue {
  data: any;
  html: string;
  preview: string;
}

export interface Params {
  required: boolean;
  unique: boolean;
  hidden: boolean;
}

export type StructureParam = Partial<Params>;

export interface Structure {
  slug: string;
  label: string;
  field_type: string;
}
